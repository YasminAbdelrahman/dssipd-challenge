# GFPGAN (Installation)


GFPGAN aims at developing a **Practical Algorithm for Real-world Face Restoration**.<br>
It leverages rich and diverse priors encapsulated in a pretrained face GAN (*e.g.*, StyleGAN2) for blind face restoration.


---

---

## Dependencies and Installation

- Python >= 3.7 (Recommend to use [Anaconda](https://www.anaconda.com/download/#linux) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html))
- [PyTorch >= 1.7](https://pytorch.org/)
- Option: NVIDIA GPU + [CUDA](https://developer.nvidia.com/cuda-downloads)
- Option: Linux

### Installation

1. Clone repo

    ```bash
    git clone https://github.com/TencentARC/GFPGAN.git
    cd GFPGAN
    ```

2. Conda Enviroment

    ```bash
    conda create -n gfpgan python=3.9
    conda activate gfpgan
    ```

3. Pytorch

    find Cuda version 
    install torch 1.10 and Cuda 11.3 from https://pytorch.org/
    
    ```bash
    nvcc --version
    conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch
    ```

4. Install dependent packages

    ```bash
    pip install basicsr

    pip install facexlib

    pip install -r requirements.txt

    python setup.py develop

    pip install realesrgan
    ```

5. Download pre-trained models

    ```bash
    choco install wget
    wget https://github.com/TencentARC/GFPGAN/releases/download/v0.2.0/GFPGANCleanv1-NoCE-C2.pth -P experiments/pretrained_models
    ```

6. Inference

    copy your images to a new folder named 'media'

    ```bash
    python inference_gfpgan.py --upscale 2 --test_path media --save_root results
    ```

---
**Results!**

Before

![BlurryDavid.ipg](./media/BlurryDavid.jpg)

After

![result.jpg](./media/result.jpg)

---

## Repo Refrence

[GFPGAN (CVPR 2021)](https://github.com/TencentARC/GFPGAN).
